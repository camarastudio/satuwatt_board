#include <HLW8012.h>
HLW8012 hlw8012;

// GPIOs
#define CF_PIN                          A0 //A0  //14
#define CF1_PIN                         13
#define SEL_PIN                         12

// Set SEL_PIN to HIGH to sample current
// This is the case for Itead's Sonoff POW, where a
// the SEL_PIN drives a transistor that pulls down
// the SEL pin in the HLW8012 when closed
#define CURRENT_MODE                    HIGH

// These are the nominal values for the resistors in the circuit
#define CURRENT_RESISTOR                0.001
#define VOLTAGE_RESISTOR_UPSTREAM       ( 5 * 470000 ) // Real: 2280k
#define VOLTAGE_RESISTOR_DOWNSTREAM     ( 1000 ) // Real 1.009k

// Basic serial communication with ESP8266
// Uses serial monitor for communication with ESP8266
//
//  Pins
//  Arduino pin 2 (RX) to ESP8266 TX
//  Arduino pin 3 to voltage divider then to ESP8266 RX
//  Connect GND from the Arduiono to GND on the ESP8266
//  Pull ESP8266 CH_PD HIGH
//
// When a command is entered in to the serial monitor on the computer 
// the Arduino will relay it to the ESP8266
//
#include <SoftwareSerial.h>
SoftwareSerial ESPserial(11, 10); // RX | TX
#define SERIAL_BAUDRATE 9600

// Sensors
#define SENSOR_DATA 2
#define SENSOR_BUFFER 128
#define READ_INTERVAL 2000 // millis
#define SEND_INTERVAL 60 // how many data will send at a time

const char *host = "api.satuwatt.com";
const char *uri = "/statistic/send";
const char *device_id = "W000001B";
const char *api_token = "5fc36eba9a967e2aabf14be9453bc73a";
const bool https = true;
const uint16_t port = 443;
const char *fingerprint = "6F 04 D9 D3 AC 56 07 DB EA BD 68 D8 C4 A0 71 E1 57 BC C0 DC"; // SHA-1 SSL fingerprint

unsigned long previousMillis = 0;
char* sensor_param[] = {"volt", "amp"};
char sensor_data[SENSOR_DATA][SENSOR_BUFFER] = {"", ""}; // Volt and Amp
uint8_t data_count = 0;

void readData() {
//    unsigned long start = millis();
    // Buffer big enough for 2 hex digits + terminating null
    char hexbuffer[3];
    // Read voltage
    unsigned int v = hlw8012.getVoltage();
    sprintf(hexbuffer, "%02X", v);
    strcat(sensor_data[0], hexbuffer);
      
  // Read ampere
  // Ampere = Watt / Volt 
    unsigned int w = hlw8012.getActivePower();
    double a = (double)w / (double)v;
    sprintf(hexbuffer, "%02X", (int)(a * 10.00));
    strcat(sensor_data[1], hexbuffer);
//    Serial.print(F("[HLW] Active Power (W)    : ")); Serial.println(w);
//    Serial.print(F("[HLW] Voltage (V)         : ")); Serial.println(v);
//    Serial.print(F("[HLW] Current (A)         : ")); Serial.println(a);
    //Serial.print("[HLW] Apparent Power (VA) : "); Serial.println(hlw8012.getApparentPower());
    //Serial.print("[HLW] Power Factor (%)    : "); Serial.println((int) (100 * hlw8012.getPowerFactor()));

    // When not using interrupts we have to manually switch to current or voltage monitor
    // This means that every time we get into the conditional we only update one of them
    // while the other will return the cached value.
    hlw8012.toggleMode();
//    Serial.print("[HLW] Read sensor time    : ");Serial.print(millis() - start); Serial.println(" (ms)");
}

void sendData(){
   // Building request string
   char request_string[512] = "";
   char request_data[512] = "";
   
   sprintf(request_string, "%s?dev_id=%s&token=%s&", uri, device_id, api_token);

   // Sensor params
   for(int i = 0; i < SENSOR_DATA; i++){
      snprintf(request_string + strlen(request_string), (sizeof request_string) - strlen(request_string), "%s=%s&", sensor_param[i], sensor_data[i]);
      strcpy(sensor_data[i], ""); // Clear sensor data
   }

   // Send data to ESP8266 via serial
   sprintf(request_data, "%s;%d;%s;%d;%s;", host, port, request_string, https, fingerprint);
//   Serial.println(request_data);
   ESPserial.print(request_data);
}
 
void setup() 
{
    Serial.begin(SERIAL_BAUDRATE);     // communication with the host computer
 
    // Start the software serial for communication with the ESP8266
    ESPserial.begin(SERIAL_BAUDRATE);
    
//    while (!ESPserial)   { 
//      Serial.println("[ARD] ESP8266 not connected"); 
//    }

    // Initialize HLW8012
    // void begin(unsigned char cf_pin, unsigned char cf1_pin, unsigned char sel_pin, unsigned char currentWhen = HIGH, bool use_interrupts = false, unsigned long pulse_timeout = PULSE_TIMEOUT);
    // * cf_pin, cf1_pin and sel_pin are GPIOs to the HLW8012 IC
    // * currentWhen is the value in sel_pin to select current sampling
    // * set use_interrupts to false, we will have to call handle() in the main loop to do the sampling
    // * set pulse_timeout to 500ms for a fast response but losing precision (that's ~24W precision :( )
//    hlw8012.begin(CF_PIN, CF1_PIN, SEL_PIN, CURRENT_MODE, false, 500000);
    hlw8012.begin(CF_PIN, CF1_PIN, SEL_PIN, CURRENT_MODE, false, 500000);
  
    // These values are used to calculate current, voltage and power factors as per datasheet formula
    // These are the nominal values for the Sonoff POW resistors:
    // * The CURRENT_RESISTOR is the 1milliOhm copper-manganese resistor in series with the main line
    // * The VOLTAGE_RESISTOR_UPSTREAM are the 5 470kOhm resistors in the voltage divider that feeds the V2P pin in the HLW8012
    // * The VOLTAGE_RESISTOR_DOWNSTREAM is the 1kOhm resistor in the voltage divider that feeds the V2P pin in the HLW8012
    hlw8012.setResistors(CURRENT_RESISTOR, VOLTAGE_RESISTOR_UPSTREAM, VOLTAGE_RESISTOR_DOWNSTREAM);
  
    // Show default (as per datasheet) multipliers
//    Serial.print(F("[HLW] Default current multiplier : ")); Serial.println(hlw8012.getCurrentMultiplier());
//    Serial.print(F("[HLW] Default voltage multiplier : ")); Serial.println(hlw8012.getVoltageMultiplier());
//    Serial.print(F("[HLW] Default power multiplier   : ")); Serial.println(hlw8012.getPowerMultiplier());
//    Serial.println(F("[ARD] Remember to to set Both NL & CR in the serial monitor."));
    Serial.println(F("[ARD] Ready"));
}

void loop() 
{
    unsigned long currentMillis = millis();
     
    // This READ_INTERVAL should be at least twice the minimum time for the current or voltage
    // signals to stabilize. Experimentally that's about 1 second.
    if (currentMillis - previousMillis >= READ_INTERVAL) {
      previousMillis = currentMillis;
      Serial.print(F("[ARD] Reading sensor data... ")); Serial.println(data_count+1);
      readData();
      data_count++;
    }
    
    // Send data
    if (data_count >= SEND_INTERVAL){
      Serial.println(F("[ARD] Sending sensor data..."));
      sendData();
      data_count = 0;
    }
     
    // listen for communication from the ESP8266 and then write it to the serial monitor
    if ( ESPserial.available() > 0 ) {
      Serial.print( "[ESP] " + ESPserial.readString() ); 
    }

//    // listen for user input and send it to the ESP8266
//    if ( Serial.available() > 0) {  ESPserial.print( Serial.readString() ); }

}
